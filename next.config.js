const withSass = require('@zeit/next-sass');
const withCSS = require('@zeit/next-css');

module.exports = withCSS(withSass({
  webpack(config, option ) {
    config.module.rules.push(
      {
        test: /\.(png|jpe?g|gif|svg|eot|ttf|woff|woff2)$/i,
        use: [
          {
            loader: 'url-loader',
          },
        ],
      }
    )
    return config
  },
  exportTrailingSlash: true
}));

// devServer: {
//   historyApiFallback: true,
//   contentBase: './',
//   hot: true
// },