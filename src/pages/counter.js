import React, { Fragment } from 'react'
import { useRouter } from 'next/router'
import { connect } from 'react-redux'
import { ofType } from 'redux-observable'

const mapStateToProps = ({ counterReducer, colorReducer }) => {
  return {
    counterReducer,
    colorReducer
  }
}

const mapDispatchToProps = dispatch => {
  return {
    inscrement: () => {
      dispatch({
        // type: 'INSCREMENT'
        type: 'PING',
        payload: 'no long cuoc'
      })
    },
    descrement: () => {
      dispatch({
        type: 'DESCREMENT'
      })
    }
  }
}

const Counter = ({ counterReducer: counter, inscrement, descrement }) => {

  const inscr = () => {
    inscrement()
    console.log("inscrement")
  }

  const descr = () => {
    descrement()
    console.log("descrement")
  }
  // const router = useRouter()
  // const { pid } = router.query
  // console.log(router.query)
  return (
    <Fragment>
      <p>Couter: {counter}</p>
      <button style={{ width: "30px" }} onClick={inscr}>+</button>
      <br />
      <button style={{ width: "30px" }} onClick={descr}>-</button>
    </Fragment>
  )
}


export default connect(mapStateToProps, mapDispatchToProps)(Counter)