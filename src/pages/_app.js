import React, { Fragment } from 'react'
import { Provider } from 'react-redux'
import store from '../redux/store'
import '../scss/styles.scss'
import '../scss/styles.css'

export default function MyApp({ Component, pageProps }) {
  return (
    <Fragment>
      <Provider store={store}>
        <Component {...pageProps} />
      </Provider>
    </Fragment>
  )
}