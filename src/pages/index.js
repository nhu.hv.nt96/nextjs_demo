import Link from 'next/link'
import { useEffect } from 'react'
import Router from 'next/router'
import { connect } from 'react-redux'



function redirect() {
  Router.replace('/about')
}

function HomePage() {
  // useEffect(() => {
  //   Router.push('/about', undefined, { shallow: true })
  // })
  return (
    <ul>
      <li>
        <Link href='/'>
          <a>Home</a>
        </Link>
      </li>
      <li>
        {/* <Link href={{pathname: "/about", query: {name: "nhuhuynh"}}}> */}
        <Link href="/about?name=nhuhuynh"><a>About Us</a>
        </Link>
      </li>
      <li>
        <Link href="/posts/[slug]" as="posts/1">
          <a>Posts 1</a>
        </Link>
      </li>
      <li>
        <Link href="/posts/[slug]" as="posts/2">
          <a>Posts 2</a>
        </Link>
      </li>
      <li>
        <Link href="/posts/[slug]" as="posts/3">
          <a>Posts 3</a>
        </Link>
      </li>
      <li>
        <Link href="/posts/[slug]" as="posts/4">
          <a>Posts 4</a>
        </Link>
      </li>
      <li>
      </li>
      <li>
        <p onClick={redirect}>About</p>
      </li>
    </ul>
  )
}

export default HomePage