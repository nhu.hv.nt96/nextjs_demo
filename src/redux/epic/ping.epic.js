import { filter, mapTo, delay, debounceTime } from 'rxjs/operators'
import { ofType } from 'redux-observable';

const pingEpic = (action$, state$) => action$.pipe(
  filter(action => {
    console.log("action", action$)
    console.log("state", state$)
    return (
      action.type === 'PING'
    )
   }),
  // ofType('PING'),
  // debounceTime(5000), 
  delay(2000),
  mapTo({ type: 'INSCREMENT' })
);

// later...
// dispatch({ type: 'PING' });

export default pingEpic