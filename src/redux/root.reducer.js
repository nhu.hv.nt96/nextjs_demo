import { combineReducers } from 'redux'
import counterReducer from './counter.reducer'
import colorReducer from './color.reducer'
const reducer = combineReducers({
    counterReducer,
    colorReducer
})

export default reducer