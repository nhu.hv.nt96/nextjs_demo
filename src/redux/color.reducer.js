
function color(state = "red", action) {
  switch (action.type) {
    case 'RED':
      return 'red'
    case 'BLUE':
      return 'blue'
    default:
      return state
  }
}

export default color