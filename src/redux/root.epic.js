import { combineEpics } from 'redux-observable';
import pingEpic from './epic/ping.epic'
const rootEpic = combineEpics(
  pingEpic
)

export default rootEpic